#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH




# Checking status of firewall
function ubuntu_check_firewall_status() {
	if [[ ! -z $(sudo ufw status | grep inactive) ]]; then
		echo -e "防火墙状态：${Green_font_prefix}已关闭${Font_color_suffix}"
	elif [[ ! -z $(sudo ufw status | grep active) ]]; then
		echo -e "防火墙状态：${Red_font_prefix}开启（建议关闭）${Font_color_suffix}"
	fi
}

# stopping firewall of ubuntu
function ubuntu_stop_firewall() {

    # 关闭防火墙
	sudo ufw disable

	if [[ ! -z $(sudo ufw status | grep inactive) ]]; then
		echo -e "${Green_font_prefix}防火墙关闭成功！${Font_color_suffix}"
		
	else
		echo -e "${Error}${Red_font_prefix}系统异常！${Font_color_suffix}"
	
	fi
	
}






